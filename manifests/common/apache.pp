# == Class: profile::common::apache
# Common apache profile
#
# === Parameters
#
# None
#
# === Variables
#
# None
#
# === Examples
#
#  include profile::common::apache
#
# === Authors
#
# Rodney Brown <rodney@punchtech.com>
#
# === Copyright
#
# Copyright 2015 Rodney Brown
#
class profile::common::apache {

  include apache

  apache::vhost { $::fqdn :
    docroot        => '/var/www/html',
    serveradmin    => "root@${::fqdn}",
    servername     => $::fqdn,
    port           => 80,
    error_log      => true,
    error_log_file => "${::fqdn}-error_log",
    vhost_name     => $::fqdn,
    scriptalias    => '/var/www/cgi-bin'
  }
}
