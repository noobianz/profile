# == Class: profile::common::printers
#
# === Parameters
#
# None
#
# === Variables
#
# None
#
# === Examples
#
#  include profile::common::printers
#
# === Authors
#
# Rodney Brown <rodney@punchtech.com>
#
# === Copyright
#
# Copyright 2015 Rodney Brown
#
class profile::common::printers {
  if $instance != undef {
    class {'printers':
      instance => $instance,
    }
  }
}
