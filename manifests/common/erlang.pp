# == Class: profile::common::erlang
#
# === Parameters
#
# None
#
# === Variables
#
# None
#
# === Examples
#
#  include profile::common::erlang
#
# === Authors
#
# Rodney Brown <rodney@punchtech.com>
#
# === Copyright
#
# Copyright 2015 Rodney Brown
#
class profile::common::erlang {
  # include erlang
  file { '/var/tmp/erlang-18.1-1.el6.x86_64.rpm':
    ensure => present,
    source => 'puppet:///test/erlang-18.1-1.el6.x86_64.rpm'
  }

  package { 'erlang':
    ensure => installed,
    provider => rpm,
    source => '/var/tmp/erlang-18.1-1.el6.x86_64.rpm',
    require => File['/var/tmp/erlang-18.1-1.el6.x86_64.rpm'],
  }
}
