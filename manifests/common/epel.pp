# == Class: profile::common::epel
#
# === Parameters
#
# None
#
# === Variables
#
# None
#
# === Examples
#
#  include profile::common::epel
#
# === Authors
#
# Rodney Brown <rodney@punchtech.com>
#
# === Copyright
#
# Copyright 2015 Rodney Brown
#
class profile::common::epel {

# Push out epel repo rpm via central bucket

  $epel = $::operatingsystemmajrelease ? {
    '6'     => 'epel-release-latest-6.noarch.rpm',
    default => 'epel-release-latest-7.noarch.rpm',
  }

  file { "/var/tmp/${epel}":
    ensure => present,
    source => "puppet:///${::environment}/${epel}",
  }

#Do a locall install of the epel rpm
  package { 'epel-release':
    ensure   => installed,
    provider => rpm,
    source   => "/var/tmp/${epel}",
    require  => File["/var/tmp/${epel}"],
  }
}
