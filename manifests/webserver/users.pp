# == Class: profile::webserver::users
#
# === Parameters
#
# None
#
# === Variables
#
# None
#
# === Examples
#
#  include profile::webserver::users
#
# === Authors
#
# Rodney Brown <rodney@punchtech.com>
#
# === Copyright
#
# Copyright 2015 Rodney Brown
#
class profile::webserver::users {
  users { webserver: }
}
