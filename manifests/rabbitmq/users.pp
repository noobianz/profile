# == Class: profile::rabbitmq::users
#
# === Parameters
#
# None
#
# === Variables
#
# None
#
# === Examples
#
#  include profile::rabbitmq::users
#
# === Authors
#
# Rodney Brown <rodney@punchtech.com>
#
# === Copyright
#
# Copyright 2015 Rodney Brown
#
class profile::rabbitmq::users {

group { 'rabbitmq':
  ensure => 'present',
  gid    => 8006,
}

users { 'rabbitmq': }
}
