# == Class: profile::rabbitmq::rabbitmq
#
# === Parameters
#
# None
#
# === Variables
#
# None
#
# === Examples
#
#  include profile::rabbitmq::rabbitmq
#
# === Authors
#
# Rodney Brown <rodney@punchtech.com>
#
# === Copyright
#
# Copyright 2015 Rodney Brown
#
class profile::rabbitmq::rabbitmq {

$rabbitmq_ver='3.5.6-1'
$rabbitmq_install_file="rabbitmq-server-${rabbitmq_ver}.noarch.rpm"
$rabbitmq_base='/data02/rabbitmq'
$rabbitmq_dirs=['/data02', '/data02/rabbitmq']
$rabbitmq_plugins=['rabbitmq_shovel', 'rabbitmq_shovel_management']

  case $::appstatus {
    'test': {
      $rabbitmq_cluster_enabled=true
      $rabbitmq_cluster_nodes=['node1', 'node2'] 
        rabbitmq_user_permissions { 'daiapp@/' :
          configure_permission => '.*',
          read_permission      => '.*',
          write_permission     => '.*',
          require => Rabbitmq_user['daiapp']
        }
    }
    default: {
      $rabbitmq_cluster_enabled=false
      $rabbitmq_node=$::hostname
        rabbitmq_user_permissions { "daiapp@${rabbitmq_node}" :
          configure_permission => '.*',
          read_permission      => '.*',
          write_permission     => '.*',
          require => Rabbitmq_user['daiapp']
        }
    }
  }
  file { $rabbitmq_dirs :
    ensure => directory,
    mode   => '0755',
    owner  => 'rabbitmq',
    group  => 'rabbitmq',
  }

  file { "/var/tmp/$rabbitmq_install_file":
    ensure => present,
    source => "puppet:///test/$rabbitmq_install_file"
  }
  rabbitmq_user { 'dai':
    ensure    => absent
  }

  rabbitmq_user { 'daiapp':
    admin    => true,
    password => 'dai',
    tags     => ['monitoring', 'tag1'],
  }

  class { 'rabbitmq':
    package_source           => "/var/tmp/$rabbitmq_install_file",
    admin_enable             => true,
    port                     => '5672',
    delete_guest_user        => false,
    config_variables         => {
      'reverse_dns_lookups' => true,
      'log_levels'          => '[{connection, info}]'
    },
    environment_variables    => {
      'RABBITMQ_BASE'        => $rabbitmq_base,
      'RABBITMQ_LOG_BASE'    => '/var/log/rabbitmq',
      'RABBITMQ_MNESIA_BASE' => "${rabbitmq_base}/mnesia"
    },
    config_cluster           => $rabbitmq_cluster_enabled,
    cluster_nodes            => $rabbitmq_cluster_nodes,
    cluster_node_type        => 'ram',
    erlang_cookie            => 'A_SECRET_COOKIE_STRING',
    wipe_db_on_cookie_change => true,
    require => Package['erlang']
}
  rabbitmq_plugin { $rabbitmq_plugins :
    ensure => 'present'
  }
}
